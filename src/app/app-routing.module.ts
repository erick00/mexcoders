import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: '' ,
    children: [
      {
        path: 'login',
        loadChildren: () => import('./components/login/login.module').then((m) => m.LoginModule)
      },
      {
        path: 'main',
        loadChildren: () => import('./components/main/main.module').then((m) => m.MainModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
