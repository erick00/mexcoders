import { Component } from '@angular/core';
import { FormControl } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  public userName = new FormControl('');
  public userPassword = new FormControl('');

  constructor(private _router: Router) {}

  public submit() {
    if (this.userName.value === 'admin@admin.com' && this.userPassword.value === 'admin') {
      this._router.navigate(['/main/products'])
    }
  }

}
