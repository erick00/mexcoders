import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

const routes: Routes = [{
  path: '',
  component: MainComponent,
  children: [{
    path: 'products',
    loadChildren: () => import('./products/products.module').then(({ProductsModule}) => ProductsModule)
  }, {
    path: 'products/create',
    loadChildren: () => import('./forms/forms.module').then(({ FormsModule }) => FormsModule)
  }]
}]

@NgModule({
  declarations: [
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    MatIconModule
  ]
})
export class MainModule { }
