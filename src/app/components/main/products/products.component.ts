import { Component, OnInit } from '@angular/core';

export interface PeriodicElement {
  id: number;
  product: string;
  description: string;
  category: string;
  price: number;
  date: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {id: 1, product: 'Coca cola', description: 'Rico bebida de cola con azucar', category: 'Refrescos', price: 15, date: '01/01/2022'},
  {id: 2, product: 'Coca cola', description: 'Rico bebida de cola sin azucar', category: 'Refrescos', price: 16.50, date: '01/01/2022'},
  {id: 3, product: 'Galletas oreo', description: 'Ricas galletas con relleno cremoso', category: 'Galletas', price: 12, date: '03/01/2022'},
];

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  public tableColumns: string[] = ['product', 'description', 'category', 'price', 'updated_date', 'actions'];
  public tableInfo = ELEMENT_DATA;
  constructor() { }

  ngOnInit(): void {
  }

}
